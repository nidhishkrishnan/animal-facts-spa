import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from './store';
import App from "./App";
import './style.css';

import routes from './routes';
import { BrowserRouter } from 'react-router-dom';


ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App routes={routes}/>
        </BrowserRouter>
    </Provider>,
document.getElementById('root'));
