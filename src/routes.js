import React, {lazy, Suspense} from 'react';


// REMOVING IN PLACE OF LAZY LOADING.
// import ResourceHome from './scripts/resourceHome';
// import Details from './scripts/Details';
// import CreateGroups from './scripts/components/CreateGroup/CreateGroups';
// import MyGroups from './scripts/myGroups';
// import MyPacks from './scripts/myPacks';

// import ApprovedApprovals from './scripts/components/Approvals/approvedApprovals';
// import PendingApprovals from './scripts/components/Approvals/pendingApprovals';
// import RejectedApprovals from './scripts/components/Approvals/rejectedApprovals';

// import ApprovedRequests from './scripts/components/Requests/approvedRequests';
// import PendingRequests from './scripts/components/Requests/pendingRequests';
// import RejectedRequests from './scripts/components/Requests/rejectedRequests';

const ResourceHome            = lazy(()   =>  import('./components/Album'));
const Cats                    = lazy(()   =>  import('./components/Cats'));
// const Details                 = lazy(()   =>  import('./scripts/details'));
// const CreateGroups            = lazy(()   =>  import('./scripts/components/CreateGroup/createGroups'));
// const MyGroups                = lazy(()   =>  import('./scripts/myGroups'));
// const MyPacks                 = lazy(()   =>  import('./scripts/myPacks'));
// const ApprovedApprovals       = lazy(()   =>  import('./scripts/components/Approvals/approvedApprovals'));
// const PendingApprovals        = lazy(()   =>  import('./scripts/components/Approvals/pendingApprovals'));
// const RejectedApprovals       = lazy(()   =>  import('./scripts/components/Approvals/rejectedApprovals'));
// const ApprovedRequests        = lazy(()   =>  import('./scripts/components/Requests/approvedRequests'));
// const PendingRequests         = lazy(()   =>  import('./scripts/components/Requests/pendingRequests'));
// const RejectedRequests        = lazy(()   =>  import('./scripts/components/Requests/rejectedRequests'));



const routes = (props) => [
  {
    path:   '/',
    main:   ResourceHome,
    exact:  true,
    isAuthenticated: false
  },
  {
    path:   '/login',
    main:   ResourceHome,
    exact:  true,
    isAuthenticated: false
  },
  {
    path:   '/cats',
    main:   Cats,
    exact:  true,
    isAuthenticated: true
  }
 /* {
    path:   '/cats',
    main:   () => <Cats {...props}/>,
    exact:  true,
    isAuthenticated: true
  }*/
];


export default routes;
