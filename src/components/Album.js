import React, {useEffect, useState} from "react";
import Button from "@material-ui/core/Button/index";
import Card from "@material-ui/core/Card/index";
import CardActions from "@material-ui/core/CardActions/index";
import CardMedia from "@material-ui/core/CardMedia/index";
import Grid from "@material-ui/core/Grid/index";
import {makeStyles} from "@material-ui/core/styles/index";
import Container from "@material-ui/core/Container/index";
import {Link} from 'react-router-dom';
import Fade from "@material-ui/core/Fade/Fade";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";

const useStyles = makeStyles(theme => ({
    icon: {
        marginRight: theme.spacing(2)
    },
    heroContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6)
    },
    heroButtons: {
        marginTop: theme.spacing(4)
    },
    cardGrid: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4)
    },
    card: {
        height: "100%",
        display: "flex",
        flexDirection: "column"
    },
    cardMedia: {
        paddingTop: "68%"//"56.25%" // 16:9
    },
    cardContent: {
        flexGrow: 1
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6)
    },
    root: {
        flexGrow: 1
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    }
}));

const Album = props => {
    const classes = useStyles();
    const {getDogImages} = props;

    const [loading, setLoading] = useState(false);
    const [dogImages, setDogImages] = useState([]);

    useEffect(() => {
        setLoading(true);
        setDogImages([]);
        getDogImages().then(response => {
            setDogImages(response.payload.data);
            setLoading(false);
        }).catch(() => {
            setLoading(false);
        })
    }, []);

    return (
        <React.Fragment>
            {loading ? <LinearProgress/> :
                <Container className={classes.cardGrid} maxWidth="lg">
                    <Grid container spacing={4}>
                        {dogImages.map((image, i) => (
                            <Fade in={true} key={i}>
                                <Grid item xs={12} sm={6} md={4}>
                                    <Card className={classes.card}>
                                        <CardMedia
                                            className={classes.cardMedia}
                                            image={image}
                                            title="Dog Image"
                                        />
                                        <CardActions>
                                            <Button component={Link} to={`/cats?image=${image}`} color="primary">
                                                View
                                            </Button>
                                        </CardActions>
                                    </Card>
                                </Grid>
                            </Fade>
                        ))}
                    </Grid>
                </Container>
            }
        </React.Fragment>
    );
};

export default Album;
