import React from 'react';
import Fade from '@material-ui/core/Fade';
import CircularProgress from '@material-ui/core/CircularProgress';

const Loader = (props) => {
    return (
        <div style={{display: 'flex', justifyContent: 'center', marginTop: '10%'}}>
            <Fade
                in={props.loading}
                style={{
                    transitionDelay: props.loading ? '800ms' : '0ms',
                }}
                unmountOnExit
            >
                <CircularProgress/>
            </Fade>
        </div>
    );
};

export default Loader;
