import React, { useState, useEffect } from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  paper: {
    padding: '1em 1em 0 1em',
    width: '750px',
  },
  dialogPaper: {
    backgroundColor: 'transparent',
    boxShadow: '0 0 0 0',
  },
});

const ModalComponent = (props) => {
  const [isModalOpen, setisModalOpen] = useState(false);
  const { handleClose, title, classes, open, flag, bflag } = props;

  /*
   * Only run if open props changes
   */
  useEffect(() => {
    setisModalOpen(open);
  }, [open]);


  return (
    <div className='modal-wrap modal'>
      <Dialog
        Classes={classes}
        maxWidth='md'
        open={isModalOpen}
        onClose={handleClose}
        // className= {bflag && 'background-manager'}
        PaperProps={{
          classes: {
            root: flag && classes.dialogPaper,
          },
        }}  
      >
        <h2 className = 'modal__title'>{title || ''}</h2>
        <div className = 'modal__close-icon'>
          {!flag && <i onClick={handleClose} className='material-icons close-icon'>close</i>}
        </div>
        <DialogContent>
          {React.cloneElement(props.component, { handleClose })}
        </DialogContent>
      </Dialog>
    </div>
  );
};
export default withStyles(styles)(ModalComponent);
