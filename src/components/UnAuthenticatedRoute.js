import React from "react";
import { Route, Redirect } from "react-router-dom";

export default function UnAuthenticatedRoute({ children, ...rest }) {
    const { isAuthenticated } = rest;
    console.log('UnAuthenticatedRoute====>', isAuthenticated);
    return (
        <Route {...rest}>
            {!isAuthenticated ? (
                children
            ) : (
                <Redirect to="/" />
            )}
        </Route>
    );
}
