import React from "react";
import {Redirect, Route, useLocation} from "react-router-dom";

export default function AuthenticatedRoute({children, ...rest}) {
    const {pathname, search} = useLocation();
    const isAuthenticated = localStorage.getItem("accessToken") !== null;
    return (
        <Route {...rest}>
            {
                isAuthenticated ? (
                    children
                ) : (
                    <Redirect to={{
                        pathname: '/login',
                        state: {from: `${pathname}${search}`}
                    }}/>
                )}
        </Route>
    );
};
