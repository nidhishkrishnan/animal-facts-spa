import React from "react";
import {Redirect, Route, useLocation} from "react-router-dom";

export default function LoginRoute({ children, ...rest }) {
    const { pathname, search } = useLocation();
    const { isAuthenticated } = rest;
    return (
        <Route {...rest}>
            {
                !isAuthenticated ? (
                children
            ) : (
                <Redirect to={
                    `/`
                } />
            )}
        </Route>
    );
}

