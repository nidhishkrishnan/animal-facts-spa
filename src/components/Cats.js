import React, {useEffect, useState} from "react";
import '../styles/cats.css';
import Fade from "@material-ui/core/Fade";
import LinearProgress from "@material-ui/core/LinearProgress/LinearProgress";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {Link} from "react-router-dom";
import Moment from "react-moment";

const Cats = props => {
    const {geCatFacts} = props;
    const [catFacts, setCatFacts] = useState({});
    const [loading, setLoading] = useState(true);
    const selectedImage = new URLSearchParams(props.location.search).get("image");

    useEffect(() => {
        geCatFacts().then(response => {
            setCatFacts(response.payload.data);
            setLoading(false);
        }).catch(() => {
            setLoading(false);
        })
    }, []);

    return (
        <React.Fragment>
            {loading ? <LinearProgress/> : <Fade in={true}>
                <div className="app">
                    <div className="details">
                        <div className="big-img">
                            <img src={selectedImage} alt=""/>
                        </div>
                        <div className="box">
                            <div className="row">
                                <h2>{catFacts.type} Fact</h2>
                            </div>
                            <p><b>Fact :</b> {catFacts.text}</p>
                            <p><b>Created on:</b> <Moment date={catFacts.createdAt} format="YYYY/MM/DD"/></p>
                            <p><b>Updated on:</b> <Moment date={catFacts.updatedAt} format="YYYY/MM/DD"/></p>
                            <p><b>Source :</b> {catFacts.source}</p>
                            <Button
                                variant="contained"
                                color="primary"
                                component={Link}
                                to="/"
                                startIcon={<ArrowBackIcon/>}>
                                Back
                            </Button>
                        </div>
                    </div>
                </div>
            </Fade>
            }
        </React.Fragment>
    );
};

export default Cats;
