import React from 'react';
import ElevationScroll from "./ElevationScroll";
import AppBar from "@material-ui/core/AppBar/AppBar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import {AccountCircle} from "@material-ui/icons";
import {Link} from 'react-router-dom';
import HomeIcon from "@material-ui/icons/Home";

const useStyles = makeStyles(theme => ({
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    },
    home: {
        flexGrow: 1
    },
    root: {
        flexGrow: 1,
    },
    buttonFontSize: {
        fontSize: "11px",
        color: "#a1a1a1"
    },
    appBar: {
        height: "50px",
        '& .MuiToolbar-regular': {
            minHeight: "50px"
        }
    },
    name: {
        marginRight: "15px"
    },
    link: {
        textTransform: "unset",
        margin: "0 20px",
        textDecoration: "unset"
    },
}));

const Header = (props) => {
    const classes = useStyles();
    const {isUserAuthenticated, signOut} = props;

    const handleSignOut = () => {
        signOut();
        props.history.push('/login');
    };

    const handleLogin = () => {
        props.history.push('/login');
    };

    return (
        <React.Fragment>
            <ElevationScroll {...props}>
                <AppBar position="static" className={classes.appBar}>
                    <Toolbar>
                        <Typography className={classes.name} variant="h6">
                            Animal Facts
                        </Typography>
                        <Button
                            className={classes.link}
                            color="inherit"
                            component={Link}
                            onClick={() => window.location.href="/"}
                            to="/"
                            startIcon={<HomeIcon/>}
                        > Home
                        </Button>
                        <Typography className={classes.title} align="right">
                            {isUserAuthenticated ? (
                                <React.Fragment>
                                    <Button
                                        color="inherit"
                                        className={classes.link}
                                        onClick={() => handleSignOut()}
                                        startIcon={<AccountCircle/>}
                                    > Logout
                                    </Button>
                                </React.Fragment>
                            ) : <Button color="inherit" className={classes.link} onClick={() => handleLogin()}
                                        startIcon={<AccountCircle/>}>Login</Button>}
                        </Typography>
                    </Toolbar>
                </AppBar>
            </ElevationScroll>
        </React.Fragment>
    );
};

export default Header;
