import React, {useState} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Paper from '@material-ui/core/Paper'
import Avatar from "@material-ui/core/Avatar";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {useHistory, useLocation} from "react-router-dom";
import LinearProgress from "@material-ui/core/LinearProgress";
import {Alert} from '@material-ui/lab';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from '@material-ui/icons/Close';
import Collapse from '@material-ui/core/Collapse';

const useStyles = makeStyles(theme => ({
    layout: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    paper: {
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(8),
            padding: `${theme.spacing(6)}px ${theme.spacing(4)}px`
        }
    },
    submit: {
        margin: theme.spacing(3, 0, 3)
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12
    }
}));

const Login = (props) => {
    const [loading, setLoading] = useState(false);
    const [open, setOpen] = useState(true);
    const [errorMessage, setErrorMessage] = useState({helperText: '', error: false});
    const [formData, setFormData] = useState({username: '', password: ''});

    let history = useHistory();
    let location = useLocation();
    let {from} = location.state || {from: {pathname: "/"}};
    const classes = useStyles({});
    const {login} = props;

    const handleSubmit = (event) => {
        event.preventDefault();
        setErrorMessage({helperText: '', error: false});
        if (formData.username && formData.password) {
            setLoading(true);
            login(formData).then(() => {
                setErrorMessage({helperText: '', error: false});
                setLoading(false);
                history.replace(from);
            }).catch(error => {
                if (error.response) {
                    setErrorMessage({helperText: `${error.response.data.message}`, error: true});
                }
                setLoading(false);
            });
        } else {
            setErrorMessage({helperText: 'Fields cannot be empty', error: true});
        }
    };
    return (
        <React.Fragment>
            {loading && <LinearProgress/>}
            <main className={classes.layout}>
                <Paper className={classes.paper} elevation={2}>
                    <Box
                        display="flex"
                        alignItems="center"
                        justifyContent="center"
                        flexDirection="column"
                    >
                        <Avatar className={classes.avatar} component={Avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5" gutterBottom>
                            Login
                        </Typography>
                        <Typography component="p" gutterBottom>
                            Log in to your account dashboard
                        </Typography>
                    </Box>
                    {errorMessage.helperText &&

                    <Collapse in={open}>
                        <Alert severity="error" action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    setOpen(false);
                                }}
                            >
                                <CloseIcon fontSize="inherit"/>
                            </IconButton>
                        }>
                            {errorMessage.helperText}
                        </Alert>
                    </Collapse>}

                    <form method="post" className={classes.form} onSubmit={handleSubmit}>
                        <TextField
                            disabled={loading}
                            error={errorMessage.error}
                            margin="normal"
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoFocus
                            defaultValue={formData.username}
                            helperText={formData.username==='' && errorMessage.error && 'Email cannot be empty'}
                            onChange={e => setFormData({...formData, username: e.target.value})}
                        />
                        <TextField
                            disabled={loading}
                            error={errorMessage.error}
                            margin="normal"
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            defaultValue={formData.password}
                            helperText={formData.password==='' && errorMessage.error && 'Password cannot be empty'}
                            onChange={e => setFormData({...formData, password: e.target.value})}
                        />
                        <Box mb={6}>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                disabled={loading}
                            >

                                {loading ? 'Signing in...' : 'Sign In'}
                                {loading && <CircularProgress size={14}/>}
                            </Button>
                        </Box>
                    </form>
                </Paper>
            </main>
        </React.Fragment>
    )
};

export default Login
