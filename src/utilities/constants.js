export const ROTATE = "ROTATE";
export const NORTH = "NORTH";
export const SOUTH = "SOUTH";
export const EAST = "EAST";
export const WEST = "WEST";

export const CHANGE_DIRECTION = "CHANGE DIRECTION";


export const SELECT_DOG_IMAGE = "SELECT_DOG_IMAGE";
export const MOVE = "MOVE";
export const LEFT = "LEFT";
export const RIGHT = "RIGHT";
export const PLACE = "PLACE";

export const REPORT_OR_TURN = "REPORT OR TURN";
export const PLACE_REGEX = /^(PLACE)\s*([0-9])\s*?,\s*?([0-9])\s*,\s*(NORTH|SOUTH|EAST|WEST)$/;

export const MOVE_REGEX = /^(MOVE)$/;
export const REPORT_REGEX = /^(REPORT)$/;
export const DIRECTION_REGEX = /^(LEFT|RIGHT|REPORT)$/;

export const INITIALIZE = "INITIALIZE";
export const LOGIN = "LOGIN";
export const SIGN_OUT = "SIGN_OUT";
export const DOG_IMAGES = "DOG_IMAGES";
export const CAT_FACTS = "CAT_FACTS";

