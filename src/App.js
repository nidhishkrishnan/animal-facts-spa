import React, {Component, Suspense} from "react";
import Album from "./components/Album";
import {appDispatch, appState} from "./redux/dispatcher/animalDispatcher";
import {connect} from "react-redux";
import {Route, Switch, withRouter} from 'react-router-dom';
import Loader from "./components/loader";
import Header from "./components/header";
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import Cats from "./components/Cats";
import Login from "./components/Login";
import LoginRoute from "./components/LoginRoute";

class App extends Component {
  componentDidMount = () => {
    this.props.initialize();
  };

  render() {
    return (
        <React.Fragment>
          <Header {...this.props} />
          <Suspense fallback={<Loader/>}>
            <Switch>
              <Route
                  path="/"
                  exact={true}
                  render={() => <Album {...this.props}/>}
              />

              <AuthenticatedRoute exact path="/cats" isAuthenticated={this.props.isUserAuthenticated}>
                {<Cats {...this.props}/>}
              </AuthenticatedRoute>

              <LoginRoute exact path="/login" isAuthenticated={this.props.isUserAuthenticated}>
                <Login {...this.props}/>
              </LoginRoute>

            </Switch>
          </Suspense>
        </React.Fragment>
    );
  }
}

const mapStateToProps = state => appState(state);

const mapDispatchToProps = dispatch => appDispatch(dispatch);

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
