import axios from "axios";
import * as Constants from "../../utilities/constants";

export const initialize = () => {
    return {
        type: Constants.INITIALIZE,
        payload: '',
    };
};

export const login = (params) => {
    let request = axios.request({
        url: 'http://localhost:8080/user/login',
        method: 'POST',
        data: params
    });
    return {
        type: Constants.LOGIN,
        payload: request,
    };
};

export const signOut = () => {
    return {
        type: Constants.SIGN_OUT,
        payload: '',
    };
};

export const getDogImages = () => {
    let request = axios.request({
        url: 'http://localhost:8080/v1/dog/images',
        method: 'GET'
    });
    return {
        type: Constants.DOG_IMAGES,
        payload: request,
    };
};

export const geCatFacts = () => {
    let request = axios.request({
        url: 'http://localhost:8080/v1/cat/facts',
        method: 'GET'
    });
    return {
        type: Constants.CAT_FACTS,
        payload: request,
    };
};
