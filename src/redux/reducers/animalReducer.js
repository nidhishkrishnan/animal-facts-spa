import * as Constant from "../../utilities/constants";
import axios from "axios";

const initialState = {
  isUserAuthenticated: false,
  images: [],
  catFacts: {}
};

const isUserAuthenticated = () => {
  const accessToken = localStorage.getItem("accessToken");
  if(accessToken !== null) {
    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    return true;
  }
  return false;
};

export default function(state = initialState, action) {
  const payload = action.payload;
  switch (action.type) {
    case Constant.INITIALIZE: {
      return {
        ...state,
        isUserAuthenticated: isUserAuthenticated()
      };
    }
    case Constant.LOGIN: {
      if(payload.status === 200) {
        const { token_type, access_token, refresh_token } = payload.data;
        axios.defaults.headers.common.Authorization = `${token_type} ${access_token}`;
        localStorage.setItem('accessToken', access_token);
        localStorage.setItem('refreshToken', refresh_token);
        return {
          ...state,
          isUserAuthenticated: true
        };
      } else {
        return state;
      }
    }
    case Constant.SIGN_OUT: {
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
      delete axios.defaults.headers.common.Authorization;
      return {
        ...state,
        isUserAuthenticated: false
      };
    }
    case Constant.DOG_IMAGES: {
      return {
        ...state,
        images: action.payload.data
      };
    }
    case Constant.CAT_FACTS: {
      return {
        ...state,
        catFacts: action.payload.data
      };
    }
    default:
      return state;
  }
}
