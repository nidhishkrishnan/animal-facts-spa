import { combineReducers } from 'redux';
import animalReducer from './animalReducer';

const rootReducer = combineReducers({
    animalReducer: animalReducer
});

export default rootReducer;
