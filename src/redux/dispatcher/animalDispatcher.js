import {
  initialize,
  login,
  signOut,
  getDogImages,
  geCatFacts
} from "../actions/animalActions";

export const appState = state => ({
  images: state.animalReducer.images,
  catFacts: state.animalReducer.catFacts,
  isUserAuthenticated: state.animalReducer.isUserAuthenticated
});

export const appDispatch = dispatch => ({
  initialize: () => dispatch(initialize()),
  getDogImages: () => dispatch(getDogImages()),
  login: credentials => dispatch(login(credentials)),
  signOut: () => dispatch(signOut()),
  geCatFacts: () => dispatch(geCatFacts())
});
