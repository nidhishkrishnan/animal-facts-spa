#### 1. How long did you spend on the homework test?
Even though I took 7 days for submission, I could spend only several hours during the weekday due to my december code freeze deliverables. Lucky I got a weekend for task completion.



#### 2. What would you add to your solution if you had more time? If you didn't spend much time on the assignment, then use this as an opportunity to explain what you would add

 - Add circuit breaker for the public API for handling fallback use cases
 - Implement HttpOnly Cookie for token management instead of storing it in the client side localstorge
 - I know the UI does not looks great, so will try to make it better with some other templates
 - Move the authorization implementation to a separate project to make it domain specific
 - Test cases for ReactJS (sorry could'nt add them since time was flying)


#### 3. Which parts did you spend the most time on?

**Server Side (Spring)**

Initially I thought of doing the authentication via https://cat-fact.herokuapp.com/ since they have integrated the Google Oauth2 auth flow, but unfortunately they have not whitelisted all redirected uris for their client id `11528929989-1c5m1aslf27i9093o2m8hvrk9gukjo8n.apps.googleusercontent.com`. 

So I end up in creating my own OAuth2 Server in the application. I have added an improved version of the Spring OAuth2 implementation from the open source project [pacman-api-auth](https://github.com/tmobile/pacbot/tree/master/api/pacman-api-auth) which I have created 2 years back for T-Mobile.

So took some time to bring all stuffs together 

**Client Side (ReactJS)**

I'm not a good UI desinger so took some time to play around with the css and brought up things together 


#### 4. How did you find the test overall? We’d love to hear your suggestions on how we can improve.

The task was really good for a Full Stack position, afterall I have enjoyed brushing up many things which I have learned before.