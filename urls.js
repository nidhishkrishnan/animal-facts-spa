module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://animal-fact-app.herokuapp.com/',
    qa: 'https://animal-fact-app.herokuapp.com/'
}